@echo off

REM Vérifie les arguments
if "%~2" == "" (
    echo Usage: %0 ^<nom_du_fichier_zip^> ^<répertoire_à_zipper^>
    exit /B 1
)

REM Nom du fichier zip
set "ZIP_FILENAME=%~1"

REM Répertoire à zipper
set "ZIP_DIRECTORY=%~2"

REM Vérifie si le répertoire existe
if not exist "%ZIP_DIRECTORY%" (
    echo Le répertoire spécifié n'existe pas.
    exit /B 1
)

REM Vérifie si 7zip est installé
where 7z >nul 2>&1
if %ERRORLEVEL% NEQ 0 (
    echo 7zip n'est pas installé. Veuillez l'installer.
    exit /B 1
)

REM Crée l'archive zip avec 7zip
7z a "%ZIP_FILENAME%" "%ZIP_DIRECTORY%"

REM Définit l'artefact de livraison dans Jenkins
echo ARCHIVE_PATH=%ZIP_FILENAME% > artifact.properties