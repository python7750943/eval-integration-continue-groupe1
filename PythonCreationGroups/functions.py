import random
import csv

def read_students(file_path):
    with open(file_path, 'r') as f_in:
        reader = csv.reader(f_in, delimiter=';')
        next(reader, None)  # Ignore la premiere ligne
        students = [(row[0], row[1]) for row in reader]
    return students

def shuffle_students(students):
    random.shuffle(students)
    return students

def create_groups(students):
    groups = []
    for i in range(0, len(students), 2):
        group = (students[i], students[i + 1]) if i + 1 < len(students) else (students[i],)
        groups.append(group)
    return groups

def write_groups_to_file(groups, output_file):
    with open(output_file, 'w', newline='') as f_out:
        f_out.write("Groupe;Eleves 1;Eleves 2\n")  # Header line
        for i, group in enumerate(groups, start=1):
            name_1 = f"{group[0][0]} {group[0][1]}".strip()
            name_2 = f"{group[1][0]} {group[1][1]}".strip() if len(group) == 2 else ''
            formatted_line = f"Groupe {i} ;{name_1};{name_2}\n"
            f_out.write(formatted_line)

