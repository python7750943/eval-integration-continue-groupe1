from functions import read_students, shuffle_students, create_groups, write_groups_to_file

if __name__ == '__main__':
    input_file = 'promotion_B3_B.csv'
    output_file = 'resultat_groupes.csv'

    students = read_students(input_file)

    shuffled_students = shuffle_students(students)

    student_groups = create_groups(shuffled_students)

    write_groups_to_file(student_groups, output_file)
